# File Creation Project

## Overview

This Java-based project allows you to create a directory named "project" with CSS and JS files. The project is executed via the command prompt, and the file structure is customized using specific commands.

## Usage

### Creating a Project Directory

To create a project directory, use the following command:

Command:-
java YourJavaClassName -p <filepath> --theme <custom/materialize/skeleton/bootstrap>

Example:-
java YourJavaClassName -p D:\Projects\MyProject --theme custom

### Themes

* custom: Custom theme (default if no theme specified)
* materialize: Materialize CSS theme
* skeleton: Skeleton CSS theme
* bootstrap: Bootstrap CSS theme

### Directory Structure

project/
|-- css/
|   |-- style.css
|-- js/
|   |-- script.js

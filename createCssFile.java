import java.io.*;
public class createCssFile{
    void cssFile(File fRead,File fWrite)throws IOException{
        int character;
        FileInputStream fin = new FileInputStream(fRead);
        FileOutputStream fos = new FileOutputStream(fWrite);
        while((character = fin.read()) != -1){
            fos.write(character);
        }
        fos.close();
        fin.close();
    }
}
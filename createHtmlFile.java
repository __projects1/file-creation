import java.io.*;
class createHtmlFile{
    void replacePath(String path) throws IOException{
        File fin = new File("D:/studylink/filecreation/Simple-File/htmlFormat.html");
        BufferedReader br = new BufferedReader(new FileReader(fin));
        String line = ""; 
        String oldtext = "";
        while((line = br.readLine()) != null){
            oldtext += line + "\n";
        }
        br.close();
        String newtext = oldtext.replaceFirst("href=\".*\"", "href=\""+path+"\"");
        FileWriter fos = new FileWriter("D:/studylink/filecreation/Simple-File/htmlFormat.html");
        fos.write(newtext);
        fos.close();
    }
    void createNormal(File fRead,File fWrite)throws IOException{
        int character;
        FileInputStream fin = new FileInputStream(fRead);
        FileOutputStream fos = new FileOutputStream(fWrite);
        while((character = fin.read()) != -1){
            fos.write(character);
        }
        fos.close();
        fin.close();
    }
}
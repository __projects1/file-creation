import java.util.regex.*;

class command {
    String extraction(String path) {
        final String extraction = "^((--theme|-p)(.+))$";
        final String pathExtracted = path;
        String extract = "";
        final Pattern p = Pattern.compile(extraction);
        Matcher m = p.matcher(pathExtracted);
        if (m.matches()) {
            MatchResult mr = m.toMatchResult();
            extract = mr.group(2);
        }
        return extract;
    }
}

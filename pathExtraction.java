import java.util.regex.*;
public class pathExtraction {
    String extraction(String path) {
        final String extraction = "^((-p)\\s+((c:|d:).*))$";
        final String pathExtracted = path;
        String extract = "";
        final Pattern p = Pattern.compile(extraction);
        Matcher m = p.matcher(pathExtracted);
        if(m.matches()) {
            MatchResult mr = m.toMatchResult();
            extract = mr.group(3);
        }
        return extract;
    }
}